package com.example.group13_finalproject;

import android.widget.TextView;
import com.example.group13_finalproject.StopWatch.Task;

public class MainActivity
    extends sofia.app.Screen
{
    TextView  total;
    TestModel model;
    TextView  timer;
    StopWatch stopWatch; 
    


    public void dButtonClicked()
    {
        model.subtractFromCount();
    }


    public void uButtonClicked()
    {
        model.addToCount();
    }


    public void initialize()
    {
        model = new TestModel();
        model.addObserver(this);
        stopWatch = new StopWatch(); 
        stopWatch.addObserver(this); 
        changeWasObserved(stopWatch); 
    }


    @Override
    public void onResume()
    {
        super.onResume();
        changeWasObserved(model);
    }




    public void onStop()
    {
        super.onStop();
        model.setCurrentCount(0);
    }


    public void changeWasObserved(TestModel tModel)
    {
        model = tModel;
        total.setText(model.getCurrentCount() + "");
        if (model.getCurrentCount() == 5)
        {
            presentScreen(Triggered.class);
        }
    }


    public void changeWasObserved(StopWatch stopWatch2)
    {
        stopWatch = stopWatch2; 
        timer.setText(stopWatch.getTime()); 
    }

}
