package com.example.group13_finalproject;

import android.media.MediaPlayer;
import android.widget.Button;
import sofia.app.Screen;

public class Triggered
    extends Screen 
{
    @Override
    protected void onStop()
    {
        // TODO Auto-generated method stub
        super.onStop();
        mediaPlayer.stop();
    }

    MediaPlayer mediaPlayer;


    @Override
    public void initialize()
    {
        // TODO Auto-generated method stub
        setContentView(R.layout.triggered);
        mediaPlayer =
            MediaPlayer.create(this.getBaseContext(), R.raw.the_other_promise);
        mediaPlayer.start();
    }
    
    public void button1Clicked() {
        presentScreen(MainActivity.class); 
        mediaPlayer.stop(); 
    }
    
}
