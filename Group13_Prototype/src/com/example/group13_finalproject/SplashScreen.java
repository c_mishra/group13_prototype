package com.example.group13_finalproject;

import android.content.Intent;
import sofia.app.ActivityStarter;
import sofia.app.Screen;

public class SplashScreen
    extends Screen
{

    @Override
    public void initialize()
    {
        setContentView(R.layout.splashscreen); 
        Thread timer = new Thread() {
            public void run()
            {
                try
                {
                    sleep(3000);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    presentScreen(MainActivity.class);
                }
            }
        };
        timer.start();
    }
}
