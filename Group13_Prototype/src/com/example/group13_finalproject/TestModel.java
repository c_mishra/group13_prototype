package com.example.group13_finalproject;

import sofia.util.Observable;

public class TestModel extends Observable
{
    int currentCount;   
    public TestModel() {
        currentCount = 0; 
        notifyObservers(); 
    }
    
    public void addToCount() {
        currentCount++; 
        notifyObservers(); 

    }
    public void setCurrentCount(int currentCount)
    {
        this.currentCount = currentCount;
    }

    public void subtractFromCount() {
        currentCount--; 
        notifyObservers(); 
    }

    public int getCurrentCount()
    {
        return currentCount;
    }
    
    
    
    
    
}
