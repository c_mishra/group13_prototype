package com.example.group13_finalproject;

import sofia.util.Timer;
import sofia.util.Observable;
import java.util.TimerTask;

public class StopWatch
    extends Observable
{
    class Task
        extends TimerTask
    {

        @Override
        public void run()
        {
            secsPassed++;
            notifyObservers();
        }

    }

    private int secsPassed;


    public StopWatch()
    {
        secsPassed = 0;
        Timer.callRepeatedly(new Task(), "run", 1000);
        notifyObservers();
    }


    public String getTime()
    {
        int hours = secsPassed / 3600; 
        int minutes = (secsPassed % 3600) / 60; 
        int seconds = (secsPassed % 60);
        return String.format("%02d:%02d:%02d",hours, minutes, seconds); 
    }

}
